REQUIRE_FIPS=
SUBDIRS=TLS RNG AES TDES HMAC SHA DSA2 ECDSA DH ECDH RSA

-include common.mk

all: check

.PHONY: check $(SUBDIRS)

check: $(SUBDIRS)

$(SUBDIRS):
	@for i in $$(ls cavs-suite/gnutls/);do \
	rm -f testvectors; \
	ln -s cavs-suite/gnutls/$$i testvectors; \
	echo running check $@ for $$i; \
	$(MAKE) $(MAKEFLAGS) -e REQUIRE_FIPS=$(REQUIRE_FIPS) -C $@ check; \
	done

clean:
	for i in $(SUBDIRS);do $(MAKE) -C $$i clean;done
